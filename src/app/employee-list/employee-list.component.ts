import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
 allData = []; 
  constructor() {  
  }

  ngOnInit() {             
         this.allData = JSON.parse(localStorage.getItem("grid_data")); 
      
  }
  
}
