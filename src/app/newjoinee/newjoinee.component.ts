import { Component, OnInit } from '@angular/core';
import { Newjoinee } from './newjoinee';
import { FormsModule,Validators } from '@angular/forms';
import { EmployeeListComponent } from '../employee-list/employee-list.component';

@Component({
    providers:[EmployeeListComponent],
  selector: 'app-newjoinee',
  templateUrl: './newjoinee.component.html',
  styleUrls: ['./newjoinee.component.css']
})
export class NewjoineeComponent implements OnInit {
    model = new Newjoinee();  
    data = [];
    onSubmit(){
       if(localStorage.getItem("grid_data") != null){
     this.data =   (localStorage.getItem("grid_data").length > 0)?(JSON.parse(localStorage.getItem("grid_data"))):{}; 
        }
     this.data.push(this.model);
     localStorage.setItem("grid_data", JSON.stringify(this.data)); 
     this.EmployeeListComponent.ngOnInit();
    }
  constructor(private EmployeeListComponent : EmployeeListComponent) { }

  ngOnInit() {     
             
  }

}
